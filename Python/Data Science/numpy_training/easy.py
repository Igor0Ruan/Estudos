#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

# Importe numpy como ‘np’ e imprima o número da versão.
print np.version.full_version
print np.version.version

# Crie uma matriz 1D com números de 0 à 9
matriz_1D = np.arange(10)
print matriz_1D

# Crie uma matriz booleana numpy 3×3 com ‘True’
matriz_2D = np.full((3, 3), True)
print matriz_2D

# Extraia todos os números ímpares de ‘arr’
arr = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
print arr[arr % 2 != 0]

# Substitua todos os números ímpares arr por -1
arr[arr % 2 != 0] = -1
print arr
