#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

# Substitua todos os números ímpares em arr com -1 sem alterar arr
arr = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
print np.place(arr, arr % 2 != 0, -1)
print arr
