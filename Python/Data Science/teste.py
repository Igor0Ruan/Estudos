import re
import requests
import json
import time
import datetime
from google.cloud import translate

translate_client = translate.Client()

target = 'pt-br'

weather_key = '311cb8f142bd8e81ac168dd6906986ad'
cidade = input('Entre com o nome da cidade: ')

req_tempo = requests.get(
    'https://api.openweathermap.org/data/2.5/weather?q='+cidade+'&appid=' + weather_key)

tempo = json.loads(req_tempo.text)

print(tempo)

if tempo['cod'] == 200:

    tempo = tempo['weather'][0]

    traduzido = translate_client.translate(
        tempo['main'], target_language=target
    )

    tempo['main'] = traduzido['translatedText']

    print("Tempo:", tempo['main'])

else:
    print(translate_client.translate(
        tempo['message'], target_language=target)['translatedText'])


# text = u'Hello World!'

# translation = translate_client.translate(
#     text, target_language=target
# )

# print(translation)

# print(u'Text: {}'.format(text))
# print(u'Translation: {}'.format(translation['translatedText']))
