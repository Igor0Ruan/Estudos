# Projeto de Estudos

Neste repositório, estou adicionando todas as disciplinas as quais aplico meus estudos.

O objetivo deste repositório é ter uma base de conhecimento solidificada para consulta e constante pesquisa e desenvolvimento, onde aplico não somente os novos conceitos adquiridos nos cursos, mas também boas práticas de código e de projetos como um todo.

## Práticas de utilização

Para este projeto, as branchs serão criadas de acordo com os capítulos sendo estudados para cada curso, onde o formato padrão é:

```disciplina/[assunto]/[parte*]/[aula*]```

Exemplo: 

Vamos assumir que foi iniciado um curso sobre *Angular* que é dividido em 3 partes e cada parte tem 5 aulas.

Para a quinta aula da segunda parte do curso, a branch que será criada será algo como:

```angular/parte2/aula5```

Caso seja um estudo, ou a criação de uma POC (Proof of Case), o que melhor se encaixaria seria:

```angular/POC-API```

As branches devem ser criadas conforme o padrão utilizado no [gitflow](https://nvie.com/posts/a-successful-git-branching-model/), portanto, é pré suposto que estes títulos de branches são classificações de features e bugfixes, por exemplo:

```feature/angular/POC-API```

Para facilitação na utilização pelo VsCode, é utilizada a extensão [gitflow](https://marketplace.visualstudio.com/items?itemName=vector-of-bool.gitflow), que trás um grande aumento na praticidade na utilização do projeto.