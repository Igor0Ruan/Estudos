export function logarTempoDeExecucao(emSegundos: boolean = false) {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const metodoOriginal = descriptor.value;
    let unidade = 'ms';
    let divisor = 1;

    if (emSegundos) {
      unidade = 's';
      divisor = 1000;
    }

    descriptor.value = function (...args:any[]) {
      console.log('-------------------');
      console.log(`Os parâmetros fornecidos ao método ${propertyKey} são: ${JSON.stringify(args, null, 1)}`);
      const t1 = performance.now();
      const retorno = metodoOriginal.apply(this, args);
      const t2 = performance.now();
      console.log(`O método ${propertyKey} retornou o valor: ${retorno}`);
      console.log(`O método ${propertyKey} foi executado em ${(t1 - t2) / divisor}${unidade}`);
      return retorno;
    }

    return descriptor;
  }
}