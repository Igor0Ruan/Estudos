import { NegociacoesView, MensagemView } from "../views/index";
import { Negociacoes, Negociacao } from "../models/index";
import { domInject, throttle } from "../helpers/decorators/index";
import { HandlerFunction, NegociacaoService } from '../services/index';
import { imprime } from '../helpers/index';

export class NegociacaoController {

  @domInject('#data')
  private _inputData: JQuery;

  @domInject('#quantidade')
  private _inputQuantidade: JQuery;

  @domInject('#valor')
  private _inputValor: JQuery;
  private _negociacoes = new Negociacoes();
  private _negociacoesView = new NegociacoesView('#negociacoesView', true);
  private _mensagemView = new MensagemView('#mensagemView');

  private _service = new NegociacaoService();

  constructor() {
    this._negociacoesView.update(this._negociacoes);
  }

  adiciona() {

    let data = new Date(this._inputData.val().replace(/-/g, ','));

    if (!this.ehDiaUtil(data)) {
      this._mensagemView.update('Somente negociações em dias úteis, por favor!');
      return;
    }

    var negociacao = new Negociacao(
      new Date(this._inputData.val().replace(/-/g, ',')),
      parseInt(this._inputQuantidade.val()),
      parseFloat(this._inputValor.val())
    );

    this._negociacoes.adiciona(negociacao);

    imprime(negociacao, this._negociacoes);

    this._negociacoesView.update(this._negociacoes);
    this._mensagemView.update('Negociação adicionada com sucesso!');
  }

  @throttle()
  importarDados() {
    const isOk: HandlerFunction = (res) => {
      if (res.ok) {
        return res;
      } else {
        throw new Error(res.statusText);
      }
    }

    this._service.obterNegociacoes(isOk)
      .then((negociacoesParaImportar: Negociacao[]) => {

        const negociacoesJaImportadas = this._negociacoes.paraArray();

        negociacoesParaImportar
          .filter(negociacao => 
            !negociacoesJaImportadas.some(jaImportada => negociacao.ehIgual(jaImportada)))
          .forEach((negociacao: Negociacao) => this._negociacoes.adiciona(negociacao));
        this._negociacoesView.update(this._negociacoes);
      });
  }

  private ehDiaUtil(data: Date) {
    return (data.getDay() != DiaDaSemana.Sabado) && (data.getDay() != DiaDaSemana.Domingo);
  }
} 

enum DiaDaSemana {
  Domingo,
  Segunda,
  Terca,
  Quarta,
  Quinta,
  Sexta,
  Sabado
}
