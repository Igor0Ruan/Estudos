const buttonRight = window.document.querySelector('.button-arrow.-right');
const buttonLeft = window.document.querySelector('.button-arrow.-left');
const elements = window.document.querySelector('.elements');

let move = 0;

buttonLeft.addEventListener('click', () => {
  move = move > 0 ? move - 100 : move;
  elements.style = `transform: translateX(-${move}px);`
});

buttonRight.addEventListener('click', () => {
  move = move < 300 ? move + 100 : move;
  console.log(move);
  elements.style = `transform: translateX(-${move}px);`
});
